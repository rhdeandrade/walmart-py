#!flask/bin/python
from datetime import timedelta
from flask import Flask, jsonify, abort, make_response, request, current_app
from functools import update_wrapper
import sqlite3, sys

app = Flask(__name__)

def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers
            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            h['Access-Control-Allow-Credentials'] = 'true'
            h['Access-Control-Allow-Headers'] = \
                "Origin, X-Requested-With, Content-Type, Accept, Authorization"
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

@app.route('/walmart/api/v1.0/tasks', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})

@app.route('/walmart/api/v1.0/maps', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def save_map():
    if not request.json or not 'map_name' in request.json:
        abort(400)

    map_name = request.json['map_name']
    routes_str = request.json['routes']
    routes = []
    for line in routes_str.split('\n'):
        l = line.split(' ')
        if len(l) == 3:
            r = {}
            r['origin'] = l[0]
            r['destiny'] = l[1]
            r['distance'] = l[2]
            routes.append(r);
        else:
            pass

    print map_name
    print routes

    insert_map(map_name, routes)
    return jsonify({'map': map_name}), 201


def insert_map(map_name, routes):
    map_id = 0
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()

    cursor.execute(" select * from maps where map_name = '" +  map_name + "';")

    row = [item[0] for item in cursor.fetchall()]

    if len(row) > 0:
        map_id = row[0]
    else:
        cursor.execute("""
            insert into maps values ((select max(map_id + 1) as id from maps), ?);
            """, (map_name,))
        conn.commit()

        cursor.execute("""
            select * from maps where map_name = ?;
        """, (map_name,))

        row = [item[0] for item in cursor.fetchall()]
        map_id = row[0]


    for r in routes:
        cursor.execute("""
            insert into routes values ((select max(route_id + 1) as id from routes), ?, ?, ?,?);
            """, (map_id, r['origin'], r['destiny'], r['distance']))
        conn.commit()

    conn.close()


def get_map_id(map_name):
    map_id = 0
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()

    cursor.execute(" select * from maps where map_name = '" +  map_name + "';")

    row = [item[0] for item in cursor.fetchall()]

    if len(row) > 0:
        map_id = row[0]

    return map_id

def get_routes(map_id):
    routes = []
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()

    cursor.execute(" select origin, destiny, distance, route_id from routes where map_id = ? ;", (map_id,))

    for item in cursor.fetchall():
        route = {}
        route['origin'] = item[0]
        route['destiny'] = item[1]
        route['distance'] = item[2]
        route['route_id'] = item[3]
        routes.append(route)

    return routes

# def evaluate_path(path):
#     conn = sqlite3.connect('database.sqlite')
#     cursor = conn.cursor()

#     for i in range(0, len(path - 2):
#         cursor.execute(" select origin, destiny, distance, route_id from routes where map_id = ? ;", (route_id,))

@app.route('/walmart/api/v1.0/route/<string:map_name>/<string:origin>/<string:destiny>/<int:autonomy>/<float:price>', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def get_route(map_name, origin, destiny, autonomy, price):
    search = True
    paths = []
    total_distance = 0
    shortest_path = []

    map_id = get_map_id(map_name)
    if map_id == 0:
        return jsonify({'error' : 'map not found'})

    routes = get_routes(map_id)

    for route in routes:
        if destiny == route['destiny']:
            path = {}
            path['distance'] = route['distance']
            path['route'] = [destiny]
            path = find_route(routes, origin, route['origin'], path,)
            paths.append(path)

    total_distance = sys.maxint
    for path in paths:
        if path['distance'] < total_distance:
            shortest_path = path['route']
            total_distance = path['distance']

    response = {}
    response['route'] = shortest_path[::-1]
    response['cost'] = total_distance / autonomy * price

    return jsonify({'route' :response})


def find_route(routes, original_origin, destiny, path):
    if original_origin == destiny:
        path['route'].append(original_origin)
        return path

    for route in routes:
        if destiny == route['destiny']:
            path['distance'] += route['distance']
            path['route'].append(destiny)
            if route['origin'] == original_origin:
                path['route'].append(original_origin)
                return path
            else:
                return find_route(routes, original_origin, route['origin'], path)

    return {}

if __name__ == '__main__':
    app.run(debug=True)