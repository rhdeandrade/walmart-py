O sistema Walmart é um Webservice REST, construído em Python v2.7.6.

Consiste basicamente em 2 métodos:
--> save_map => [POST] [hostname]/walmart/api/v1.0/maps
        Chamada:
        {
            'map_name' : "nome do mapa",
            'routes' :   "A B 10
                             B C 20
                             C D 20"
        }
        Os parâmetros devem seguir essa estrutur, caso contrário o mapa não será salvo.
        O nome do Mapa deve ser único, caso o Nome do Mapa já exista, o WebService irá apenas adicionar as rotas ao Mapa com o nome informado.

--> get_route => [GET] [hostname]/walmart/api/v1.0/route/<string:map_name>/<string:origin>/<string:destiny>/<int:autonomy>/<float:price>
    Chamada: [hostname]/walmart/api/v1.0/route/<string:map_name>/<string:origin>/<string:destiny>/<int:autonomy>/<float:price>
    Parâmetros:
        <string:map_name>: Nome do mapa a ser procurado, texto
        <string:origin>: Ponto de origem, texto
        <string:destiny>: Ponto de destino, texto
        <int:autonomy>: Autonomia, número inteiro
        <float:price>: Preço do combustível, número real

    O sistema pegará a rota mais barata e devolverá no seguinte formato:
    Resposta:
    {
        "route": {
            "cost": 17.5,
            "route": [
                "A",
                "F",
                "G"
            ]
        }
    }


Para iniciar o serviço basta executar o arquivo "app.py"

O serviço estará disponível no endereço: http://[hostname]:5000/walmart/api/v1.0/